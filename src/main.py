from logica.modelo import Modelo
from presentacion.vista import GraphEditor as Vista


if __name__ == "__main__":
    modelo = Modelo()
    vista = Vista(modelo)
    vista.run()
