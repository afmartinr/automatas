import random
import matplotlib.pyplot as plt
from matplotlib.patches import FancyArrowPatch, Arc
from matplotlib.widgets import Button, TextBox
from tkinter.simpledialog import askstring

from logica.transicion import Transicion


class GraphEditor:
    def __init__(self, modelo):
        self.modelo = modelo

        self.mode = "draw"
        self.elements = []
        self.lines = []
        self.start_element = None

        self.fig, self.ax = plt.subplots()

        # Definir la escala del lienzo
        self.ax.set_xlim(0, 10)
        self.ax.set_ylim(0, 10)
        # Quitar coordenadas en 'x' y 'y'
        self.ax.set_xticks([])
        self.ax.set_yticks([])

        self.ax.set_aspect("equal", adjustable="box")

        self.btn_draw = Button(plt.axes([0.8, 0.025, 0.1, 0.04]), "Dibujar/Conectar")
        self.text_box = TextBox(
            plt.axes([0.25, 0.025, 0.5, 0.05]), "Cadena", textalignment="center"
        )

    def toggle_mode(self, event):
        self.mode = "connect" if self.mode == "draw" else "draw"
        print(f"Switched to {self.mode} mode.")

    def on_click(self, event):
        if event.inaxes != self.ax:
            return
        
        if self.mode == "draw":
            circle = self.modelo.agregar_nodo(
                button=event.button, xdata=event.xdata, ydata=event.ydata, ax=self.ax
            )
            self.elements.append(circle)
            self.ax.annotate(
                circle.title,  # Texto del título
                xy=circle.circle.center,  # Coordenadas del punto donde se coloca el título
                ha="center",  # Alineación horizontal del título
                va="bottom",  # Alineación vertical del título
                fontsize=12,  # Tamaño de fuente del título
                color="black",  # Color del título
            )
            self.ax.figure.canvas.draw()
        elif self.mode == "connect":
            if self.start_element is None:
                for element in self.elements:
                    if element.circle.contains(event)[0]:
                        self.start_element = element
                        break
            else:
                end_element = None
                for element in self.elements:
                    if element.circle.contains(event)[0]:
                        end_element = element
                        break
                if end_element:
                    # Generar componentes RGB aleatorios
                    r = random.uniform(0.2, 0.7)
                    g = random.uniform(0.2, 0.7)
                    b = random.uniform(0.2, 0.7)
                    color_random = (r, g, b)

                    valor = askstring("Ingresar dato", "Por favor, ingrese un dato:")
                    linea = Transicion(self.start_element, end_element, valor)

                    for line in self.lines:
                        if (
                            line.start_element == linea.start_element
                            and line.end_element == linea.end_element
                        ):
                            print("inicia igual")
                            if not valor in line.parameter:
                                line.set_parameter(f"{line.parameter},{valor}")
                                print(line.parameter)
                            line.text.remove()
                            linea = line
                    if linea not in self.lines:
                        self.lines.append(linea)
                    if self.start_element.circle == end_element.circle:
                        # Calcular el centro y los ángulos para el semicírculo
                        arc_center = (
                            (
                                self.start_element.circle.center[0]
                                + end_element.circle.center[0]
                            )
                            / 2,
                            self.start_element.circle.center[1] + 1,
                        )
                        arc_radius = self.start_element.circle.r * 0.70
                        angle_start = 335
                        angle_end = 210

                        # Crear un objeto Arc para el semicírculo
                        arc = Arc(
                            arc_center,  # Coordenadas del centro del arco
                            2 * arc_radius,  # Ancho del arco
                            2 * arc_radius,  # Altura del arco
                            theta1=angle_start,  # Ángulo de inicio
                            theta2=angle_end,  # Ángulo de fin
                            color=color_random,  # Color del arco
                            lw=2,  # Grosor de la línea
                        )
                        nota = self.ax.annotate(
                            linea.parameter,  # Texto del título
                            xy=(
                                arc.center
                            ),  # Coordenadas del punto donde se coloca el título
                            ha="center",  # Alineación horizontal del título
                            va="center",  # Alineación vertical del título
                            fontsize=12,  # Tamaño de fuente del título
                            color=color_random,  # Color del título
                        )
                        linea.set_text(nota)
                        # Agregar el arco y la flecha al objeto de ejes
                        self.ax.add_patch(arc)
                        self.start_element = None
                    else:
                        arrow = FancyArrowPatch(
                            (self.start_element.circle.center),  # Coordenadas de inicio
                            (end_element.circle.center),  # Coordenadas de fin
                            arrowstyle="-|>",  # Estilo de la flecha
                            mutation_scale=15,  # Tamaño de la cabeza de la flecha
                            lw=2,  # Grosor de la línea
                            color=color_random,  # Color de la flecha
                            connectionstyle="arc3,rad=0.3",  # Estilo de conexión con el texto en la mitad
                        )
                        nota = self.ax.annotate(
                            linea.parameter,  # Texto a mostrar
                            xy=(
                                self.start_element.circle.center[0],
                                self.start_element.circle.center[1],
                            ),  # Coordenadas de inicio
                            xytext=(
                                (
                                    (
                                        self.start_element.circle.center[0]
                                        + end_element.circle.center[0]
                                    )
                                    / 2
                                )
                                + random.uniform(-0.5, 0.5),
                                (
                                    (
                                        self.start_element.circle.center[1]
                                        + end_element.circle.center[1]
                                    )
                                    / 2
                                )
                                + random.uniform(-0.5, 0.5),
                            ),  # Coordenadas de inicio
                            ha="center",  # Alineación horizontal del texto en la mitad de la flecha
                            va="center",  # Alineación vertical del texto en la mitad de la flecha
                            fontsize=12,  # Tamaño de fuente del texto
                            color=color_random,  # Color del texto
                        )
                        self.start_element = None
                        self.ax.add_patch(arrow)
                        linea.set_text(nota)
                    self.ax.figure.canvas.draw()

    def run(self):
        self.btn_draw.on_clicked(self.toggle_mode)
        self.text_box.on_submit(
            lambda valor: self.modelo.cargar_y_verificar(
                self.elements, self.lines, valor
            )
        )

        self.ax.figure.canvas.mpl_connect("button_press_event", self.on_click)
        plt.show()
