class Transicion:
    def __init__(self, start, end, parameter):
        self.start_element = start
        self.end_element = end
        self.parameter = parameter

    def set_parameter(self, param):
        self.parameter = param

    def set_text(self, nota):
        self.text = nota