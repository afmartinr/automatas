from matplotlib.patches import Wedge


class Circle:
    def __init__(self, center, radius, ax, title, fc="b"):
        self.title = title
        self.circle = Wedge(center, radius, 0, 360, width=0.1, fc=fc, alpha=0.5)
        ax.add_patch(self.circle)


class DoubleCircle(Circle):
    def __init__(self, center, radius, gap, ax, title):
        super().__init__(center, radius, ax, title, fc="g")

        self.circle_ext = Wedge(
            center, radius + gap, 0, 360, width=gap + 0.2, fc="g", alpha=0.5
        )
        ax.add_patch(self.circle_ext)
