from tkinter import messagebox
from automata.fa.nfa import NFA
import automata.base.config as global_config  # es para poder habilitar la mutacion de los automadas

from logica.nodos import DoubleCircle, Circle


# Modelo
class Modelo:
    def __init__(self):
        global_config.should_validate_automata = (
            False  # esta cosa es lo que permite mutaciones
        )
        self.automata = None
        self.num_nodos = 0

    def agregar_nodo(self, button, xdata, ydata, ax):
        if button == 1:
            circle = Circle(
                (xdata, ydata),
                radius=1,
                ax=ax,
                title=f"q{self.num_nodos}",
            )
        elif button == 3:
            circle = DoubleCircle(
                (xdata, ydata),
                radius=1,
                gap=-0.29,
                ax=ax,
                title=f"q{self.num_nodos}",
            )
        self.num_nodos += 1
        return circle

    def cargar_automata(self, nodes, lines):
        estados = []
        estados_finales = []
        abecedario = []
        transiciones = {}

        for nodo in nodes:
            estados.append(nodo.title)
            if isinstance(nodo, DoubleCircle):
                estados_finales.append(nodo.title)

        for line in lines:
            estado_actual = line.start_element.title
            if not estado_actual in transiciones.keys():
                transiciones[estado_actual] = {}

            simbolos_entrada = line.parameter.split(",")

            for simbolo in simbolos_entrada:
                # Ingresa los simbolos en el abecedario
                if simbolo not in abecedario:
                    abecedario.append(simbolo)
                # Ingresa las transiciones para cada símbolo de entrada
                if not simbolo in transiciones[estado_actual].keys():
                    transiciones[estado_actual][simbolo] = set([])
                transiciones[estado_actual][simbolo].add(line.end_element.title)
        self.automata = NFA(
            states=set(estados),
            input_symbols=set(abecedario),
            transitions=transiciones,
            initial_state="q0",
            final_states=set(estados_finales),
        )
        # print(f"estados={estados}\nabecedario={abecedario}\ntransiciones{transiciones}\nestados_finales={estados_finales}")
        print("Automata valido")

    def verificar(self, valor):
        print(f"Verificando '{valor}'")
        if self.automata.accepts_input(valor):
            messagebox.showinfo(
                "Exitoso!!!", f"La cadena '{valor}' cumple con las reglas del automata."
            )
        else:
            messagebox.showerror(
                "Lo sentimos", "El texto ingresado NO cumple las reglas del automata."
            )

    def cargar_y_verificar(self, nodes, lines, valor):
        """
            Cargar el automata y verificar el valor ingresado
        """
        self.cargar_automata(nodes, lines)
        self.verificar(valor)