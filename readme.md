Universidad Francisco José de Caldas
Facultad de Ingeniería
Ciencias de la Computación III

# Link video
https://youtu.be/A9f0eB50AMo

# Link repositorio GitLab
https://gitlab.com/afmartinr/automatas

# Miembros del grupo
- Sergio Camilo Sierra Pinilla 20201020072 
- Oscar Julian Rojas Muñoz 20201020080 
- Andrés Felipe Martín Rodríguez 20201020137 
